/**
 * An application class which provides advanced configuration for special character flags which modify an Actor
 * @extends {BaseEntitySheet}
 */
export default class ActorSheetFlags extends BaseEntitySheet {
  static get defaultOptions() {
    const options = super.defaultOptions;
    return mergeObject(options, {
      id: "actor-flags",
	    classes: ["dnd"],
      template: "systems/dnd/templates/apps/actor-flags.html",
      width: 500,
      closeOnSubmit: true
    });
  }

  /* -------------------------------------------- */

  /**
   * Configure the title of the special traits selection window to include the Actor name
   * @type {String}
   */
  get title() {
    return `${game.i18n.localize('DND.FlagsTitle')}: ${this.object.name}`;
  }

  /* -------------------------------------------- */

  /**
   * Prepare data used to render the special Actor traits selection UI
   * @return {Object}
   */
  getData() {
    const data = super.getData();
    data.actor = this.object;
    data.flags = this._getFlags();
    data.bonuses = this._getBonuses();
    return data;
  }

  /* -------------------------------------------- */

  /**
   * Prepare an object of flags data which groups flags by section
   * Add some additional data for rendering
   * @return {Object}
   */
  _getFlags() {
    const flags = {};
    for ( let [k, v] of Object.entries(CONFIG.DND.characterFlags) ) {
      if ( !flags.hasOwnProperty(v.section) ) flags[v.section] = {};
      let flag = duplicate(v);
      flag.type = v.type.name;
      flag.isCheckbox = v.type === Boolean;
      flag.isSelect = v.hasOwnProperty('choices');
      flag.value = this.entity.getFlag("dnd", k);
      flags[v.section][`flags.dnd.${k}`] = flag;
    }
    return flags;
  }

  /* -------------------------------------------- */

  /**
   * Get the bonuses fields and their localization strings
   * @return {Array}
   * @private
   */
  _getBonuses() {
    const bonuses = [
      {name: "data.bonuses.mwak.attack", label: "DND.BonusMWAttack"},
      {name: "data.bonuses.mwak.damage", label: "DND.BonusMWDamage"},
      {name: "data.bonuses.rwak.attack", label: "DND.BonusRWAttack"},
      {name: "data.bonuses.rwak.damage", label: "DND.BonusRWDamage"},
      {name: "data.bonuses.msak.attack", label: "DND.BonusMSAttack"},
      {name: "data.bonuses.msak.damage", label: "DND.BonusMSDamage"},
      {name: "data.bonuses.rsak.attack", label: "DND.BonusRSAttack"},
      {name: "data.bonuses.rsak.damage", label: "DND.BonusRSDamage"},
      {name: "data.bonuses.abilities.check", label: "DND.BonusAbilityCheck"},
      {name: "data.bonuses.abilities.save", label: "DND.BonusAbilitySave"},
      {name: "data.bonuses.abilities.skill", label: "DND.BonusAbilitySkill"},
      {name: "data.bonuses.spell.dc", label: "DND.BonusSpellDC"}
    ];
    for ( let b of bonuses ) {
      b.value = getProperty(this.object.data, b.name) || "";
    }
    return bonuses;
  }

  /* -------------------------------------------- */

  /**
   * Update the Actor using the configured flags
   * Remove/unset any flags which are no longer configured
   */
  async _updateObject(event, formData) {
    const actor = this.object;
    const updateData = expandObject(formData);

    // Unset any flags which are "false"
    let unset = false;
    const flags = updateData.flags.dnd;
    for ( let [k, v] of Object.entries(flags) ) {
      if ( [undefined, null, "", false, 0].includes(v) ) {
        delete flags[k];
        if ( hasProperty(actor.data.flags, `dnd.${k}`) ) {
          unset = true;
          flags[`-=${k}`] = null;
        }
      }
    }

    // Apply the changes
    await actor.update(updateData, {diff: false});
  }
}
