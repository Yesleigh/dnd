// Namespace Configuration Values
export const DND = {};

// ASCII Artwork
DND.ASCII = `_______________________________
______      ______ _____ _____ 
|  _  \\___  |  _  \\  ___|  ___|
| | | ( _ ) | | | |___ \\| |__  
| | | / _ \\/\\ | | |   \\ \\  __| 
| |/ / (_>  < |/ //\\__/ / |___ 
|___/ \\___/\\/___/ \\____/\\____/
_______________________________`;


/**
 * The set of Ability Scores used within the system
 * @type {Object}
 */
DND.abilities = {
  "str": "DND.AbilityStr",
  "dex": "DND.AbilityDex",
  "con": "DND.AbilityCon",
  "int": "DND.AbilityInt",
  "wis": "DND.AbilityWis",
  "cha": "DND.AbilityCha"
};

/* -------------------------------------------- */

/**
 * Character alignment options
 * @type {Object}
 */
DND.alignments = {
  'lg': "DND.AlignmentLG",
  'ng': "DND.AlignmentNG",
  'cg': "DND.AlignmentCG",
  'ln': "DND.AlignmentLN",
  'tn': "DND.AlignmentTN",
  'cn': "DND.AlignmentCN",
  'le': "DND.AlignmentLE",
  'ne': "DND.AlignmentNE",
  'ce': "DND.AlignmentCE"
};


DND.weaponProficiencies = {
  "sim": "DND.WeaponSimpleProficiency",
  "mar": "DND.WeaponMartialProficiency"
};

DND.toolProficiencies = {
  "art": "DND.ToolArtisans",
  "disg": "DND.ToolDisguiseKit",
  "forg": "DND.ToolForgeryKit",
  "game": "DND.ToolGamingSet",
  "herb": "DND.ToolHerbalismKit",
  "music": "DND.ToolMusicalInstrument",
  "navg": "DND.ToolNavigators",
  "pois": "DND.ToolPoisonersKit",
  "thief": "DND.ToolThieves",
  "vehicle": "DND.ToolVehicle"
};


/* -------------------------------------------- */

/**
 * This Object defines the various lengths of time which can occur
 * @type {Object}
 */
DND.timePeriods = {
  "inst": "DND.TimeInst",
  "turn": "DND.TimeTurn",
  "round": "DND.TimeRound",
  "minute": "DND.TimeMinute",
  "hour": "DND.TimeHour",
  "day": "DND.TimeDay",
  "month": "DND.TimeMonth",
  "year": "DND.TimeYear",
  "perm": "DND.TimePerm",
  "spec": "DND.Special"
};


/* -------------------------------------------- */

/**
 * This describes the ways that an ability can be activated
 * @type {Object}
 */
DND.abilityActivationTypes = {
  "none": "DND.None",
  "action": "DND.Action",
  "bonus": "DND.BonusAction",
  "reaction": "DND.Reaction",
  "minute": DND.timePeriods.minute,
  "hour": DND.timePeriods.hour,
  "day": DND.timePeriods.day,
  "special": DND.timePeriods.spec,
  "legendary": "DND.LegAct",
  "lair": "DND.LairAct",
  "crew": "DND.VehicleCrewAction"
};

/* -------------------------------------------- */


DND.abilityConsumptionTypes = {
  "ammo": "DND.ConsumeAmmunition",
  "attribute": "DND.ConsumeAttribute",
  "material": "DND.ConsumeMaterial",
  "charges": "DND.ConsumeCharges"
};


/* -------------------------------------------- */

// Creature Sizes
DND.actorSizes = {
  "tiny": "DND.SizeTiny",
  "sm": "DND.SizeSmall",
  "med": "DND.SizeMedium",
  "lg": "DND.SizeLarge",
  "huge": "DND.SizeHuge",
  "grg": "DND.SizeGargantuan"
};

DND.tokenSizes = {
  "tiny": 1,
  "sm": 1,
  "med": 1,
  "lg": 2,
  "huge": 3,
  "grg": 4
};

/* -------------------------------------------- */

/**
 * Classification types for item action types
 * @type {Object}
 */
DND.itemActionTypes = {
  "mwak": "DND.ActionMWAK",
  "rwak": "DND.ActionRWAK",
  "msak": "DND.ActionMSAK",
  "rsak": "DND.ActionRSAK",
  "save": "DND.ActionSave",
  "heal": "DND.ActionHeal",
  "abil": "DND.ActionAbil",
  "util": "DND.ActionUtil",
  "other": "DND.ActionOther"
};

/* -------------------------------------------- */

DND.itemCapacityTypes = {
  "items": "DND.ItemContainerCapacityItems",
  "weight": "DND.ItemContainerCapacityWeight"
};

/* -------------------------------------------- */

/**
 * Enumerate the lengths of time over which an item can have limited use ability
 * @type {Object}
 */
DND.limitedUsePeriods = {
  "sr": "DND.ShortRest",
  "lr": "DND.LongRest",
  "day": "DND.Day",
  "charges": "DND.Charges"
};


/* -------------------------------------------- */

/**
 * The set of equipment types for armor, clothing, and other objects which can ber worn by the character
 * @type {Object}
 */
DND.equipmentTypes = {
  "light": "DND.EquipmentLight",
  "medium": "DND.EquipmentMedium",
  "heavy": "DND.EquipmentHeavy",
  "bonus": "DND.EquipmentBonus",
  "natural": "DND.EquipmentNatural",
  "shield": "DND.EquipmentShield",
  "clothing": "DND.EquipmentClothing",
  "trinket": "DND.EquipmentTrinket",
  "vehicle": "DND.EquipmentVehicle"
};


/* -------------------------------------------- */

/**
 * The set of Armor Proficiencies which a character may have
 * @type {Object}
 */
DND.armorProficiencies = {
  "lgt": DND.equipmentTypes.light,
  "med": DND.equipmentTypes.medium,
  "hvy": DND.equipmentTypes.heavy,
  "shl": "DND.EquipmentShieldProficiency"
};


/* -------------------------------------------- */

/**
 * Enumerate the valid consumable types which are recognized by the system
 * @type {Object}
 */
DND.consumableTypes = {
  "ammo": "DND.ConsumableAmmunition",
  "potion": "DND.ConsumablePotion",
  "poison": "DND.ConsumablePoison",
  "food": "DND.ConsumableFood",
  "scroll": "DND.ConsumableScroll",
  "wand": "DND.ConsumableWand",
  "rod": "DND.ConsumableRod",
  "trinket": "DND.ConsumableTrinket"
};

/* -------------------------------------------- */

/**
 * The valid currency denominations supported by the 5e system
 * @type {Object}
 */
DND.currencies = {
  "pp": "DND.CurrencyPP",
  "gp": "DND.CurrencyGP",
  "ep": "DND.CurrencyEP",
  "sp": "DND.CurrencySP",
  "cp": "DND.CurrencyCP",
};

/* -------------------------------------------- */


// Damage Types
DND.damageTypes = {
  "acid": "DND.DamageAcid",
  "bludgeoning": "DND.DamageBludgeoning",
  "cold": "DND.DamageCold",
  "fire": "DND.DamageFire",
  "force": "DND.DamageForce",
  "lightning": "DND.DamageLightning",
  "necrotic": "DND.DamageNecrotic",
  "piercing": "DND.DamagePiercing",
  "poison": "DND.DamagePoison",
  "psychic": "DND.DamagePsychic",
  "radiant": "DND.DamageRadiant",
  "slashing": "DND.DamageSlashing",
  "thunder": "DND.DamageThunder"
};

/* -------------------------------------------- */

DND.distanceUnits = {
  "none": "DND.None",
  "self": "DND.DistSelf",
  "touch": "DND.DistTouch",
  "ft": "DND.DistFt",
  "mi": "DND.DistMi",
  "spec": "DND.Special",
  "any": "DND.DistAny"
};

/* -------------------------------------------- */


/**
 * Configure aspects of encumbrance calculation so that it could be configured by modules
 * @type {Object}
 */
DND.encumbrance = {
  currencyPerWeight: 50,
  strMultiplier: 15,
  vehicleWeightMultiplier: 2000 // 2000 lbs in a ton
};

/* -------------------------------------------- */

/**
 * This Object defines the types of single or area targets which can be applied
 * @type {Object}
 */
DND.targetTypes = {
  "none": "DND.None",
  "self": "DND.TargetSelf",
  "creature": "DND.TargetCreature",
  "ally": "DND.TargetAlly",
  "enemy": "DND.TargetEnemy",
  "object": "DND.TargetObject",
  "space": "DND.TargetSpace",
  "radius": "DND.TargetRadius",
  "sphere": "DND.TargetSphere",
  "cylinder": "DND.TargetCylinder",
  "cone": "DND.TargetCone",
  "square": "DND.TargetSquare",
  "cube": "DND.TargetCube",
  "line": "DND.TargetLine",
  "wall": "DND.TargetWall"
};


/* -------------------------------------------- */


/**
 * Map the subset of target types which produce a template area of effect
 * The keys are DND target types and the values are MeasuredTemplate shape types
 * @type {Object}
 */
DND.areaTargetTypes = {
  cone: "cone",
  cube: "rect",
  cylinder: "circle",
  line: "ray",
  radius: "circle",
  sphere: "circle",
  square: "rect",
  wall: "ray"
};


/* -------------------------------------------- */

// Healing Types
DND.healingTypes = {
  "healing": "DND.Healing",
  "temphp": "DND.HealingTemp"
};


/* -------------------------------------------- */


/**
 * Enumerate the denominations of hit dice which can apply to classes
 * @type {Array.<string>}
 */
DND.hitDieTypes = ["d6", "d8", "d10", "d12"];


/* -------------------------------------------- */

/**
 * Character senses options
 * @type {Object}
 */
DND.senses = {
  "bs": "DND.SenseBS",
  "dv": "DND.SenseDV",
  "ts": "DND.SenseTS",
  "tr": "DND.SenseTR"
};


/* -------------------------------------------- */

/**
 * The set of skill which can be trained
 * @type {Object}
 */
DND.skills = {
  "acr": "DND.SkillAcr",
  "ani": "DND.SkillAni",
  "arc": "DND.SkillArc",
  "ath": "DND.SkillAth",
  "dec": "DND.SkillDec",
  "his": "DND.SkillHis",
  "ins": "DND.SkillIns",
  "itm": "DND.SkillItm",
  "inv": "DND.SkillInv",
  "med": "DND.SkillMed",
  "nat": "DND.SkillNat",
  "prc": "DND.SkillPrc",
  "prf": "DND.SkillPrf",
  "per": "DND.SkillPer",
  "rel": "DND.SkillRel",
  "slt": "DND.SkillSlt",
  "ste": "DND.SkillSte",
  "sur": "DND.SkillSur"
};


/* -------------------------------------------- */

DND.spellPreparationModes = {
  "always": "DND.SpellPrepAlways",
  "atwill": "DND.SpellPrepAtWill",
  "innate": "DND.SpellPrepInnate",
  "pact": "DND.PactMagic",
  "prepared": "DND.SpellPrepPrepared"
};

DND.spellUpcastModes = ["always", "pact", "prepared"];


DND.spellProgression = {
  "none": "DND.SpellNone",
  "full": "DND.SpellProgFull",
  "half": "DND.SpellProgHalf",
  "third": "DND.SpellProgThird",
  "pact": "DND.SpellProgPact",
  "artificer": "DND.SpellProgArt"
};

/* -------------------------------------------- */

/**
 * The available choices for how spell damage scaling may be computed
 * @type {Object}
 */
DND.spellScalingModes = {
  "none": "DND.SpellNone",
  "cantrip": "DND.SpellCantrip",
  "level": "DND.SpellLevel"
};

/* -------------------------------------------- */


/**
 * Define the set of types which a weapon item can take
 * @type {Object}
 */
DND.weaponTypes = {
  "simpleM": "DND.WeaponSimpleM",
  "simpleR": "DND.WeaponSimpleR",
  "martialM": "DND.WeaponMartialM",
  "martialR": "DND.WeaponMartialR",
  "natural": "DND.WeaponNatural",
  "improv": "DND.WeaponImprov",
  "siege": "DND.WeaponSiege"
};


/* -------------------------------------------- */

/**
 * Define the set of weapon property flags which can exist on a weapon
 * @type {Object}
 */
DND.weaponProperties = {
  "amm": "DND.WeaponPropertiesAmm",
  "hvy": "DND.WeaponPropertiesHvy",
  "fin": "DND.WeaponPropertiesFin",
  "fir": "DND.WeaponPropertiesFir",
  "foc": "DND.WeaponPropertiesFoc",
  "lgt": "DND.WeaponPropertiesLgt",
  "lod": "DND.WeaponPropertiesLod",
  "rch": "DND.WeaponPropertiesRch",
  "rel": "DND.WeaponPropertiesRel",
  "ret": "DND.WeaponPropertiesRet",
  "spc": "DND.WeaponPropertiesSpc",
  "thr": "DND.WeaponPropertiesThr",
  "two": "DND.WeaponPropertiesTwo",
  "ver": "DND.WeaponPropertiesVer"
};


// Spell Components
DND.spellComponents = {
  "V": "DND.ComponentVerbal",
  "S": "DND.ComponentSomatic",
  "M": "DND.ComponentMaterial"
};

// Spell Schools
DND.spellSchools = {
  "abj": "DND.SchoolAbj",
  "con": "DND.SchoolCon",
  "div": "DND.SchoolDiv",
  "enc": "DND.SchoolEnc",
  "evo": "DND.SchoolEvo",
  "ill": "DND.SchoolIll",
  "nec": "DND.SchoolNec",
  "trs": "DND.SchoolTrs"
};

// Spell Levels
DND.spellLevels = {
  0: "DND.SpellLevel0",
  1: "DND.SpellLevel1",
  2: "DND.SpellLevel2",
  3: "DND.SpellLevel3",
  4: "DND.SpellLevel4",
  5: "DND.SpellLevel5",
  6: "DND.SpellLevel6",
  7: "DND.SpellLevel7",
  8: "DND.SpellLevel8",
  9: "DND.SpellLevel9"
};

// Spell Scroll Compendium UUIDs
DND.spellScrollIds = {
  0: 'Compendium.dnd.items.rQ6sO7HDWzqMhSI3',
  1: 'Compendium.dnd.items.9GSfMg0VOA2b4uFN',
  2: 'Compendium.dnd.items.XdDp6CKh9qEvPTuS',
  3: 'Compendium.dnd.items.hqVKZie7x9w3Kqds',
  4: 'Compendium.dnd.items.DM7hzgL836ZyUFB1',
  5: 'Compendium.dnd.items.wa1VF8TXHmkrrR35',
  6: 'Compendium.dnd.items.tI3rWx4bxefNCexS',
  7: 'Compendium.dnd.items.mtyw4NS1s7j2EJaD',
  8: 'Compendium.dnd.items.aOrinPg7yuDZEuWr',
  9: 'Compendium.dnd.items.O4YbkJkLlnsgUszZ'
};

/**
 * Define the standard slot progression by character level.
 * The entries of this array represent the spell slot progression for a full spell-caster.
 * @type {Array[]}
 */
DND.SPELL_SLOT_TABLE = [
  [2],
  [3],
  [4, 2],
  [4, 3],
  [4, 3, 2],
  [4, 3, 3],
  [4, 3, 3, 1],
  [4, 3, 3, 2],
  [4, 3, 3, 3, 1],
  [4, 3, 3, 3, 2],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 2, 1, 1]
];

/* -------------------------------------------- */

// Polymorph options.
DND.polymorphSettings = {
  keepPhysical: 'DND.PolymorphKeepPhysical',
  keepMental: 'DND.PolymorphKeepMental',
  keepSaves: 'DND.PolymorphKeepSaves',
  keepSkills: 'DND.PolymorphKeepSkills',
  mergeSaves: 'DND.PolymorphMergeSaves',
  mergeSkills: 'DND.PolymorphMergeSkills',
  keepClass: 'DND.PolymorphKeepClass',
  keepFeats: 'DND.PolymorphKeepFeats',
  keepSpells: 'DND.PolymorphKeepSpells',
  keepItems: 'DND.PolymorphKeepItems',
  keepBio: 'DND.PolymorphKeepBio',
  keepVision: 'DND.PolymorphKeepVision'
};

/* -------------------------------------------- */

/**
 * Skill, ability, and tool proficiency levels
 * Each level provides a proficiency multiplier
 * @type {Object}
 */
DND.proficiencyLevels = {
  0: "DND.NotProficient",
  1: "DND.Proficient",
  0.5: "DND.HalfProficient",
  2: "DND.Expertise"
};

/* -------------------------------------------- */

/**
 * The amount of cover provided by an object.
 * In cases where multiple pieces of cover are
 * in play, we take the highest value.
 */
DND.cover = {
  0: 'DND.None',
  .5: 'DND.CoverHalf',
  .75: 'DND.CoverThreeQuarters',
  1: 'DND.CoverTotal'
}

/* -------------------------------------------- */


// Condition Types
DND.conditionTypes = {
  "blinded": "DND.ConBlinded",
  "charmed": "DND.ConCharmed",
  "deafened": "DND.ConDeafened",
  "diseased": "DND.ConDiseased",
  "exhaustion": "DND.ConExhaustion",
  "frightened": "DND.ConFrightened",
  "grappled": "DND.ConGrappled",
  "incapacitated": "DND.ConIncapacitated",
  "invisible": "DND.ConInvisible",
  "paralyzed": "DND.ConParalyzed",
  "petrified": "DND.ConPetrified",
  "poisoned": "DND.ConPoisoned",
  "prone": "DND.ConProne",
  "restrained": "DND.ConRestrained",
  "stunned": "DND.ConStunned",
  "unconscious": "DND.ConUnconscious"
};

// Languages
DND.languages = {
  "common": "DND.LanguagesCommon",
  "aarakocra": "DND.LanguagesAarakocra",
  "abyssal": "DND.LanguagesAbyssal",
  "aquan": "DND.LanguagesAquan",
  "auran": "DND.LanguagesAuran",
  "celestial": "DND.LanguagesCelestial",
  "deep": "DND.LanguagesDeepSpeech",
  "draconic": "DND.LanguagesDraconic",
  "druidic": "DND.LanguagesDruidic",
  "dwarvish": "DND.LanguagesDwarvish",
  "elvish": "DND.LanguagesElvish",
  "giant": "DND.LanguagesGiant",
  "gith": "DND.LanguagesGith",
  "gnomish": "DND.LanguagesGnomish",
  "goblin": "DND.LanguagesGoblin",
  "gnoll": "DND.LanguagesGnoll",
  "halfling": "DND.LanguagesHalfling",
  "ignan": "DND.LanguagesIgnan",
  "infernal": "DND.LanguagesInfernal",
  "orc": "DND.LanguagesOrc",
  "primordial": "DND.LanguagesPrimordial",
  "sylvan": "DND.LanguagesSylvan",
  "terran": "DND.LanguagesTerran",
  "cant": "DND.LanguagesThievesCant",
  "undercommon": "DND.LanguagesUndercommon"
};

// Character Level XP Requirements
DND.CHARACTER_EXP_LEVELS =  [
  0, 300, 900, 2700, 6500, 14000, 23000, 34000, 48000, 64000, 85000, 100000,
  120000, 140000, 165000, 195000, 225000, 265000, 305000, 355000]
;

// Challenge Rating XP Levels
DND.CR_EXP_LEVELS = [
  10, 200, 450, 700, 1100, 1800, 2300, 2900, 3900, 5000, 5900, 7200, 8400, 10000, 11500, 13000, 15000, 18000,
  20000, 22000, 25000, 33000, 41000, 50000, 62000, 75000, 90000, 105000, 120000, 135000, 155000
];

// Configure Optional Character Flags
DND.characterFlags = {
  "powerfulBuild": {
    name: "DND.FlagsPowerfulBuild",
    hint: "DND.FlagsPowerfulBuildHint",
    section: "Racial Traits",
    type: Boolean
  },
  "savageAttacks": {
    name: "DND.FlagsSavageAttacks",
    hint: "DND.FlagsSavageAttacksHint",
    section: "Racial Traits",
    type: Boolean
  },
  "elvenAccuracy": {
    name: "DND.FlagsElvenAccuracy",
    hint: "DND.FlagsElvenAccuracyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "halflingLucky": {
    name: "DND.FlagsHalflingLucky",
    hint: "DND.FlagsHalflingLuckyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "initiativeAdv": {
    name: "DND.FlagsInitiativeAdv",
    hint: "DND.FlagsInitiativeAdvHint",
    section: "Feats",
    type: Boolean
  },
  "initiativeAlert": {
    name: "DND.FlagsAlert",
    hint: "DND.FlagsAlertHint",
    section: "Feats",
    type: Boolean
  },
  "jackOfAllTrades": {
    name: "DND.FlagsJOAT",
    hint: "DND.FlagsJOATHint",
    section: "Feats",
    type: Boolean
  },
  "observantFeat": {
    name: "DND.FlagsObservant",
    hint: "DND.FlagsObservantHint",
    skills: ['prc','inv'],
    section: "Feats",
    type: Boolean
  },
  "reliableTalent": {
    name: "DND.FlagsReliableTalent",
    hint: "DND.FlagsReliableTalentHint",
    section: "Feats",
    type: Boolean
  },
  "remarkableAthlete": {
    name: "DND.FlagsRemarkableAthlete",
    hint: "DND.FlagsRemarkableAthleteHint",
    abilities: ['str','dex','con'],
    section: "Feats",
    type: Boolean
  },
  "weaponCriticalThreshold": {
    name: "DND.FlagsCritThreshold",
    hint: "DND.FlagsCritThresholdHint",
    section: "Feats",
    type: Number,
    placeholder: 20
  }
};

// Configure allowed status flags
DND.allowedActorFlags = [
  "isPolymorphed", "originalActor"
].concat(Object.keys(DND.characterFlags));
